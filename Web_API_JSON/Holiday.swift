//
//  Holiday.swift
//  Web_API_JSON
//
//  Created by Amol Tamboli on 24/03/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import Foundation

struct HolidayResponse : Decodable {
    var response : Holidays
}

struct Holidays : Decodable {
    var holidays : [HolidaysDetail]
}

struct HolidaysDetail : Decodable {
    var name : String
    var country : CountryInfo
    var date : DateInfo
}

struct CountryInfo : Decodable {
    var id : String
    var name : String
}

struct DateInfo : Decodable {
    var iso : String
    var datetime : DateTimeInfo
}

struct DateTimeInfo : Decodable {
    var year : Int?
    var month : Int?
    var day : Int?
}
