//
//  HolidayRequest.swift
//  Web_API_JSON
//
//  Created by Amol Tamboli on 24/03/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import Foundation

enum HolidayError : Error {
    case noDataAvailable
    case canNotProcessData
}
struct HolidayRequest {
    let resourceURL : URL
    let API_KEY = "7546998b96cfd1b782b4c385e2785c37f11e058a"
    
    init(countryCode:String) {
        
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy"
        let currentYear = format.string(from: date)
        
        let resourceString = "https://calendarific.com/api/v2/holidays?api_key=\(API_KEY)&country=\(countryCode)&year=\(currentYear)"
        guard let resourceURL = URL(string: resourceString) else {fatalError()}
        
        self.resourceURL = resourceURL
    }
        func getHolidays (completion: @escaping (Result<[HolidaysDetail], HolidayError>) -> Void) {
            let dataTask = URLSession.shared.dataTask(with: resourceURL) { data, _, _ in
                guard let jsondata = data else {
                    completion(.failure(.noDataAvailable))
                    return
                }
                
                do {
                    let decoder = JSONDecoder()
                    let holidayResponse = try decoder.decode(HolidayResponse.self, from: jsondata)
                    let holidayDetails = holidayResponse.response.holidays
                    completion(.success(holidayDetails))
                } catch {
                    completion(.failure(.canNotProcessData))
                }
            }
            dataTask.resume()
        }
    }

